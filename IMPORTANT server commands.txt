#Mod specific server commands have been changed as follows;

get_force_default_armor #returns if players are allowed to use all game items, or just troop specific items (default 1 = all items)
get_combat_gold_bonus #returns score gold bonus (default 1 per score point earned within every minute)
get_round_gold_bonus #returns tick gold bonus (default 100 every minute)
get_starting_gold #returns starting gold (default 600)

set_force_default_armor <1 or 0> #sets if players are allowed to use all game items, or just troop specific items (1 = all items, 0 = troop items)
set_combat_gold_bonus <value> #sets score gold bonus ratio (0-10)
set_round_gold_bonus <value> #sets round gold bonus ratio (0-10000)
set_starting_gold <value> #sets starting gold (0-1000)
